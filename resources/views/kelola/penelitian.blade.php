@extends('template')

@section('main')
	<h2>Data Penelitian</h2>
	<div class="text-right"><button type="button" class="btn btn-outline-success" data-toggle="modal" data-target=".tambah">Tambah</button><div>
	<br>
	<div class="table-responsive">
		<table class="table table-hover table-bordered text-center">
		  	<thead class="table-primary">
			    <tr>
				    <th width="2%">#</th>
				    <th>Tema</th>
				    <th>Judul</th>
				    <th>Deskripsi</th>
				    <th>Tanggal</th>
				    <th>Tempat</th>
				    <th width="4%">Tahun</th>
				    <th>Sumber Pembiayaan</th>
				    <th>Biaya</th>
				    <th>Evaluasi</th>
				    <th>Integrasi</th>
				    <th width="4%">File</th>
				    <th width="12%">Action</th>
			    </tr>
		  	</thead>
		  	<tbody>
			    <tr>
			    	<th class="align-middle" scope="row">1</th>
			    	<td class="align-middle">ESQ Berbagi</td>
			    	<td class="align-middle">ACT</td>
			    	<td class="align-middle">Wilayah Jakarta Selatan</td>
			    	<td class="align-middle">2019</td>
			    	<td class="align-middle">Jumat, 1 Januari 2019</td>
			    	<td class="align-middle">2019</td>
			    	<td class="align-middle">Blablabla</td>
			    	<td class="align-middle">Blablabla</td>
			    	<td class="align-middle">Blablabla</td>
			    	<td class="align-middle">Blablabla</td>
			    	<td>
			    		<button type="button" class="btn btn-sm btn-outline-primary">Info</button>
			    	</td>
			    	<td>
						<button type="button" class="btn btn-sm btn-outline-warning" data-toggle="modal" data-target=".edit">Edit</button>
						<button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target=".delete">Delete</button>
						<button type="button" class="btn btn-sm btn-outline-info">Print</button>
			    	</td>
			    </tr>
		  	</tbody>
		</table>
	</div>
@stop
	
	{{--Start Modal Tambah--}}
	<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header bg-success">
		        	<h5 class="modal-title" id="exampleModalLongTitle">Tambah Data</h5>
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          		<span aria-hidden="true">&times;</span>
		 			</button>
		      	</div>
		      	<div class="modal-body">
		        	...
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        	<button type="button" class="btn btn-success">Save changes</button>
		      	</div>
		    </div>
	  	</div>
	</div>
	{{--End Modal Tambah--}}

	{{--Start Modal Edit--}}
	<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header bg-warning">
		        	<h5 class="modal-title" id="exampleModalLongTitle">Edit Data</h5>
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          		<span aria-hidden="true">&times;</span>
		 			</button>
		      	</div>
		      	<div class="modal-body">
		        	...
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        	<button type="button" class="btn btn-warning">Save changes</button>
		      	</div>
		    </div>
	  	</div>
	</div>
	{{--End Modal Edit--}}

	{{--Start Modal Delete--}}
	<div class="modal fade delete" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header bg-danger">
		        	<h5 class="modal-title" id="exampleModalLongTitle">Delete Data</h5>
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          		<span aria-hidden="true">&times;</span>
		 			</button>
		      	</div>
		      	<div class="modal-body">
		        	...
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancle</button>
		        	<button type="button" class="btn btn-danger">Delete</button>
		      	</div>
		    </div>
	  	</div>
	</div>
	{{--End Modal Delete--}}
