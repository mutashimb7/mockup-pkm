@extends('template')

@section('main')
	<h2>Data Kurikulum</h2>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<div class="table-responsive">
				<table class="table table-hover table-bordered text-center">
				  	<thead>
				  		<tr class="table-primary">
				  			<th class="align-middle" colspan="7">Semester 1</th>
				  		</tr>
					    <tr class="">
						    <th class="align-middle" rowspan="2" width="10%">Kode</th>
						    <th class="align-middle" rowspan="2">Matakuliah</th>
						    <th colspan="2">SKS</th>
						    <th class="align-middle" rowspan="2" width="17%">Jenis</th>
					    </tr>
					    <tr class="">
						    <th width="4%">Teori</th>
						    <th width="4%">Lab</th>
					    </tr>
				  	</thead>
				  	<tbody>
					    <tr>
					    	<td class="align-middle" scope="row"><a href="#" class="text-dark">CSSE303</a></td>
					    	<td class="align-middle text-left"><a href="#" class="text-dark">Rekayasa Perangkat Lunak</a></td>
					    	<td class="align-middle">3</td>
					    	<td class="align-middle">0</td>
					    	<td class="align-middle">Wajib</td>
					    </tr>
					    <tr>
					    	<td class="align-middle" scope="row"><a href="#" class="text-dark">KKES201</a></td>
					    	<td class="align-middle text-left"><a href="#" class="text-dark">Analisis dan Perancangan Berorientasi Objek</a></td>
					    	<td class="align-middle">2</td>
					    	<td class="align-middle">1</td>
					    	<td class="align-middle">Wajib</td>
					    </tr>
				  	</tbody>
				</table>
			</div><br>
		</div>
		<div class="col-sm-6">
			<div class="table-responsive">
				<table class="table table-hover table-bordered text-center">
				  	<thead>
				  		<tr class="table-primary">
				  			<th class="align-middle" colspan="7">Semester 1</th>
				  		</tr>
					    <tr class="">
						    <th class="align-middle" rowspan="2" width="10%">Kode</th>
						    <th class="align-middle" rowspan="2">Matakuliah</th>
						    <th colspan="2">SKS</th>
						    <th class="align-middle" rowspan="2" width="17%">Jenis</th>
					    </tr>
					    <tr class="">
						    <th width="4%">Teori</th>
						    <th width="4%">Lab</th>
					    </tr>
				  	</thead>
				  	<tbody>
					    <tr>
					    	<td class="align-middle" scope="row"><a href="#" class="text-dark">CSSE303</a></td>
					    	<td class="align-middle text-left"><a href="#" class="text-dark">Rekayasa Perangkat Lunak</a></td>
					    	<td class="align-middle">3</td>
					    	<td class="align-middle">0</td>
					    	<td class="align-middle">Wajib</td>
					    </tr>
					    <tr>
					    	<td class="align-middle" scope="row"><a href="#" class="text-dark">KKES201</a></td>
					    	<td class="align-middle text-left"><a href="#" class="text-dark">Analisis dan Perancangan Berorientasi Objek</a></td>
					    	<td class="align-middle">2</td>
					    	<td class="align-middle">1</td>
					    	<td class="align-middle">Wajib</td>
					    </tr>
				  	</tbody>
				</table>
			</div><br>
		</div>
	</div>
@stop