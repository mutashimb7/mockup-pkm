@extends('template')

@section('main')
	<div class="login container">
		<!--br>
		{!! Form::open(['url' => 'login']) !!}
		  	<div class="form-group">
			    {!! Form::label('emailaddress', 'Email Address') !!}
			    {!! Form::text('email', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-group">
			    {!! Form::label('password', 'Password') !!}
			    {!! Form::text('password', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-check">
			    {!! Form::checkbox('remmember', null, ['class' => 'form-check-input']) !!}
			    {!! Form::label('rememberme', 'Remember me', ['class' => 'form-check-label']) !!}
		  	</div>
			    {!! Form::submit('Login', ['class' => 'btn btn-primary float-right']) !!}
		{!! Form::close() !!}<br-->
		<br>
		<form action="{{ url('login') }}" method="POST">
			@csrf
		  	<div class="form-group">
			    <label>Email address</label>
			    <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email" name="email">
		  	</div>
		  	<div class="form-group">
			    <label>Password</label>
			    <input type="password" class="form-control" placeholder="Password" name="password">
		  	</div>
		  	<div class="form-check">
			    <input type="checkbox" class="form-check-input" name="remember">
			    <label class="form-check-label">Remember me</label>
		  	</div>
		  	<button type="submit" class="btn btn-primary float-right">Login</button>
		</form>
	</div>
@stop