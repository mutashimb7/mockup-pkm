@extends('template')

@section('main')
	<div class="container"><br><br>
		<div class="row">
			<div class="col-sm-12">
				<h1>National Accounting Challange, EBS Raih Prestasi Diajang Kompetisi Ini</h1>
				<h6 class="text-secondary">By Fulan | November 10, 2019 | TAGs</h6>
				<br><br>
			</div>
			<div class="col-sm-9">
				<img class="card-img-top" src="https://esqbs.ac.id/wp-content/uploads/2017/09/6.-National-Accounting-Challange-EBS-Raih-Prestasi-Diajang-Kompetisi-Ini.jpeg" alt="Card image cap">
				<br><br>
				<b>National Accounting Challange, EBS Raih Prestasi Diajang Kompetisi Ini</b>
				<br><br>
				<div class="text-justify">
					<p>ESQ Business School – Rabu, 6 september 2017 adalah hari ke-2 dari kegiatan Welcoming Week 2017. Acara tersebut diselenggarakan di kampus ESQ Business School (EBS), jl. T.B. Simatupang Kav 1, Cilandak, Menara 165. Dihari yang sama, kampus EBS mempunyai agenda selain dari menggelar acara Welcoming Week 2017 yaitu mengikuti sebuah kompetisi bergengsi setiap 2 tahun sekali di indonesia.</p>
					<p>National Accounting Challange adalah perlombaan akuntasi terbesar di Indonesia yang diadakan setiap 2 tahun sekali. Tempat penyelenggaraan acara bergengsi tersebut dilaksanakan di kampus Sekolah Akuntansi Negara (STAN) Kementerian Keuangan RI yang terletak di jl. Bintaro Utama Sektor 5a, Tangerang Selatan, Banten. EBS mengirimkan tiga mahasiswanya untuk ikut bersaing dalam kompetisi akuntansi. Dalam National Accounting Challange 2017, EBS mendapatkan pengalaman yang sangat berharga, dikarenakan ini adalah pertama kali mnegikuti kompetisi bergengesi di Indonesia dan mendapat presitasi hingga bisa menduduki peringkat 14 dari 161 Univeristas yang mengikuti kompetisi tersebut. Dan EBS satu satu peserta yang berasal dari jurusan managemen.</p>
					<p>Hasil yang membanggakan yang diciptakan oleh mahasiswa EBS berupa prestasi yang bisa mencapai peringkat 14 dari 161 peserta (universitas). Kampus ESQ Business School memiliki 9 keunggulan, salah satu keunggulan yang telah membuahkan hasil terhadap mahasiswanya yaitu penilaian personal core values mahasiswa secara periodik.</p>
				</div>
			</div>
			<div class="col-sm-3">
				<h6><b class="text-secondary">Recent Post</b></h6>
				<a href="{{ url('details')}}"><p>Kolaborasi Dosen Trainer dan Mentor Didik Mahasiswa EBS agar memiliki Kompetensi dan Karakter Unggul</p></a>
				<a href="{{ url('details')}}">Indonesia Butuh Calon Pemimpin Berkarakter, EBS Berupaya Melahirkan Lulusannya</p></a>
				<a href="{{ url('details')}}">Mempelajari Design Thinking Secara Intensif dengan "Workshop Series: Design Spirit"</p></a>
			</div>
		</div>
	</div>
@stop