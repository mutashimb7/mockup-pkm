@extends('template')

@section('main')
	@if (!empty($sub_halaman) && $sub_halaman == 'prestasi-dosen')
		<h2>Data Prestasi Dosen</h2>
	@elseif (!empty($sub_halaman) && $sub_halaman == 'prestasi-mahasiswa')
		<h2>Data Prestasi Mahasiswa</h2>
	@elseif (!empty($sub_halaman) && $sub_halaman == 'pengabdian-masyarakat')
		<h2>Data Pengabdian Masyarakat</h2>
	@elseif (!empty($sub_halaman) && $sub_halaman == 'penelitian')
		<h2>Data Penelitian</h2>
	@elseif (!empty($sub_halaman) && $sub_halaman == 'hasil-kerjasama')
		<h2>Data Hasil Kerjasama</h2>
	@endif
	<br><br>
	<div class="row">
		<div class="col-sm-3">
			<div class="card" style="width: 22rem;">
				<a href="{{ $url }}">
					<img class="card-img-top" src="https://esqtours.com/wp-content/uploads/2018/05/ESQ-Tours-Travel-Menara-165.jpg" alt="Card image cap">
				</a>
				 <div class="card-body">
				    <h5 class="card-title">Judul Blog</h5>
				    <p class="card-text">Sedikit deskripsi untuk menjelaskan isi dari blog ini.</p>
				  </div>
			</div><br>
		</div>
	</div>
	
@stop