<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="{{ url('/')}}">Mockup PKM</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

      @if (!empty($halaman) && $halaman == 'kelola')
        <li class="nav-item dropdown active">
      @else
        <li class="nav-item dropdown">
      @endif
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kelola Data
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @if (!empty($halaman) && $sub_halaman == 'k-kurikulum')
              <a class="dropdown-item active" href="{{ url('kelola/kurikulum') }}">Data Kurikulum</a>
            @else
              <a class="dropdown-item" href="{{ url('kelola/kurikulum') }}">Data Kurikulum</a>
            @endif

            @if (!empty($halaman) && $sub_halaman == 'k-prestasi-dosen')
              <a class="dropdown-item active" href="{{ url('kelola/prestasi-dosen') }}">Data Prestasi Dosen</a>
            @else
              <a class="dropdown-item" href="{{ url('kelola/prestasi-dosen') }}">Data Prestasi Dosen</a>
            @endif

            @if (!empty($halaman) && $sub_halaman == 'k-prestasi-mahasiswa')
              <a class="dropdown-item active" href="{{ url('kelola/prestasi-mahasiswa') }}">Data Prestasi Mahasiswa</a>
            @else
              <a class="dropdown-item" href="{{ url('kelola/prestasi-mahasiswa') }}">Data Prestasi Mahasiswa</a>
            @endif

            @if (!empty($halaman) && $sub_halaman == 'k-pengabdian-masyarakat')
              <a class="dropdown-item active" href="{{ url('kelola/pengabdian-masyarakat') }}">Data Pengabdian Masyarakat</a>
            @else
              <a class="dropdown-item" href="{{ url('kelola/pengabdian-masyarakat') }}">Data Pengabdian Masyarakat</a>
            @endif

            @if (!empty($halaman) && $sub_halaman == 'k-penelitian')
              <a class="dropdown-item active" href="{{ url('kelola/penelitian') }}">Data Penelitian</a>
            @else
              <a class="dropdown-item" href="{{ url('kelola/penelitian') }}">Data Penelitian</a>
            @endif

            @if (!empty($halaman) && $sub_halaman == 'k-hasil-kerjasama')
              <a class="dropdown-item active" href="{{ url('kelola/hasil-kerjasama') }}">Data Hasil Kejasama</a>
            @else
              <a class="dropdown-item" href="{{ url('kelola/hasil-kerjasama') }}">Data Hasil Kerjasama</a>
            @endif
          </div>
        </li>

      @if (!empty($halaman) && $sub_halaman == 'kurikulum')
        <li class="nav-item active"><a class="nav-link" href="{{ url('pages/kurikulum') }}">Kurikulum</a></li>
      @else
        <li class="nav-item"><a class="nav-link" href="{{ url('pages/kurikulum') }}">Kurikulum</a></li>
      @endif

      @if (!empty($halaman) && $sub_halaman == 'prestasi-dosen' || $sub_halaman == 'prestasi-mahasiswa')
        <li class="nav-item dropdown active">
      @else
        <li class="nav-item dropdown">
      @endif
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Prestasi
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @if (!empty($halaman) && $sub_halaman == 'prestasi-dosen')
              <a class="dropdown-item active" href="{{ url('pages/prestasi-dosen') }}">Prestasi Dosen</a>
            @else
              <a class="dropdown-item" href="{{ url('pages/prestasi-dosen') }}">Prestasi Dosen</a>
            @endif

            @if (!empty($halaman) && $sub_halaman == 'prestasi-mahasiswa')
              <a class="dropdown-item active" href="{{ url('pages/prestasi-mahasiswa') }}">Prestasi Mahasiswa</a>
            @else
              <a class="dropdown-item" href="{{ url('pages/prestasi-mahasiswa') }}">Prestasi Mahasiswa</a>
            @endif
          </div>
        </li>

      @if (!empty($halaman) && $sub_halaman == 'pengabdian-masyarakat')
        <li class="nav-item active"><a class="nav-link" href="{{ url('pages/pengabdian-masyarakat') }}">Pengabdian Masyarakat</a></li>
      @else
        <li class="nav-item"><a class="nav-link" href="{{ url('pages/pengabdian-masyarakat') }}">Pengabdian Masyarakat</a></li>
      @endif

      @if (!empty($halaman) && $sub_halaman == 'penelitian')
        <li class="nav-item active"><a class="nav-link" href="{{ url('pages/penelitian') }}">Penelitian</a></li>
      @else
        <li class="nav-item"><a class="nav-link" href="{{ url('pages/penelitian') }}">Penelitian</a></li>
      @endif

      @if (!empty($halaman) && $sub_halaman == 'hasil-kerjasama')
        <li class="nav-item active"><a class="nav-link" href="{{ url('pages/hasil-kerjasama') }}">Hasil Kerjasama</a></li>
      @else
        <li class="nav-item"><a class="nav-link" href="{{ url('pages/hasil-kerjasama') }}">Hasil Kerjasama</a></li>
      @endif
    </ul>

    @if (!empty($halaman) && $halaman == 'login')
      <a href="{{ url('login') }}"><button class="btn btn-primary my-2 my-sm-0" type="button">Login</button></a>
    @else
      <a href="{{ url('login') }}"><button class="btn btn-outline-primary my-2 my-sm-0" type="button">Login</button></a>
    @endif
  </div>
</nav><br><br><br>