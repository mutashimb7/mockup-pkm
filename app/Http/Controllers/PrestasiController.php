<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrestasiController extends Controller
{
	public function index_dosen() {
    	return view('pages.blog');
    }

    public function index_mahasiswa() {
        return view('pages.blog');
    }

    public function keloladata_dosen() {
    	return view('kelola.prestasi-dosen');
    }

    public function keloladata_mahasiswa() {
    	return view('kelola.prestasi-mahasiswa');
    }
}
