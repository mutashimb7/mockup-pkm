<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Login;

class UserController extends Controller
{
    public function users() {
    	$no = 1;
    	$halaman = 'users';
    	$mhs = Login::all()->sortBy('level');
    	return view('mhs.index', compact('halaman', 'mhs', 'no'));
    }

    public function store(Request $request) {
    	Login::create($request->all());
    	return redirect('users');
    }
}
