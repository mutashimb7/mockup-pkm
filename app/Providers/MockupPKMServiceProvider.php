<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class MockupPKMServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $halaman = '';
        $sub_halaman = '';
        $url = '';

        if(Request::segment(1) == 'kelola') {
            $halaman = 'kelola';

            if(Request::segment(2) == 'kurikulum') {
                $sub_halaman = 'k-kurikulum';
            }

            if(Request::segment(2) == 'prestasi-dosen') {
                $sub_halaman = 'k-prestasi-dosen';
            }

            if(Request::segment(2) == 'prestasi-mahasiswa') {
                $sub_halaman = 'k-prestasi-mahasiswa';
            }

            if(Request::segment(2) == 'pengabdian-masyarakat') {
                $sub_halaman = 'k-pengabdian-masyarakat';
            }

            if(Request::segment(2) == 'penelitian') {
                $sub_halaman = 'k-penelitian';
            }

            if(Request::segment(2) == 'hasil-kerjasama') {
                $sub_halaman = 'k-hasil-kerjasama';
            }
        }

        if(Request::segment(1) == 'pages') {
            $halaman = 'pages';

            if(Request::segment(2) == 'kurikulum') {
                $sub_halaman = 'kurikulum';
            }

            if(Request::segment(2) == 'prestasi-dosen') {
                $sub_halaman = 'prestasi-dosen';
            }

            if(Request::segment(2) == 'prestasi-mahasiswa') {
                $sub_halaman = 'prestasi-mahasiswa';
            }

            if(Request::segment(2) == 'pengabdian-masyarakat') {
                $sub_halaman = 'pengabdian-masyarakat';
            }

            if(Request::segment(2) == 'penelitian') {
                $sub_halaman = 'penelitian';
            }

            if(Request::segment(2) == 'hasil-kerjasama') {
                $sub_halaman = 'hasil-kerjasama';
            }
            $url = $sub_halaman . "/details";
        }

        if(Request::segment(1) == 'login') {
            $halaman = 'login';
        }
        
        view()->share('halaman', $halaman);
        view()->share('sub_halaman', $sub_halaman);
        view()->share('url', $url);
    }

    public function register()
    {
        //
    }
}
