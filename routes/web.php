<?php
//PagesView
Route::get('/', 'PagesController@homepage');
Route::get('login', 'PagesController@login');
Route::get('details', 'PagesController@details');

//KelolaView
Route::get('kelola/kurikulum', 'KurikulumController@keloladata');
Route::get('kelola/prestasi-dosen', 'PrestasiController@keloladata_dosen');
Route::get('kelola/prestasi-mahasiswa', 'PrestasiController@keloladata_mahasiswa');
Route::get('kelola/pengabdian-masyarakat', 'PengabdianMasyarakatController@keloladata');
Route::get('kelola/penelitian', 'PenelitianController@keloladata');
Route::get('kelola/hasil-kerjasama', 'HasilKerjasamaController@keloladata');

//PengunjungView
Route::get('pages/kurikulum', 'KurikulumController@index');
Route::get('pages/prestasi-dosen', 'PrestasiController@index_dosen');
Route::get('pages/prestasi-mahasiswa', 'PrestasiController@index_mahasiswa');
Route::get('pages/pengabdian-masyarakat', 'PengabdianMasyarakatController@index');
Route::get('pages/penelitian', 'PenelitianController@index');
Route::get('pages/hasil-kerjasama', 'HasilKerjasamaController@index');

//DetailsView
Route::get('pages/kurikulum/details', 'PagesController@details');
Route::get('pages/prestasi-dosen/details', 'PagesController@details');
Route::get('pages/prestasi-mahasiswa/details', 'PagesController@details');
Route::get('pages/pengabdian-masyarakat/details', 'PagesController@details');
Route::get('pages/penelitian/details', 'PagesController@details');
Route::get('pages/hasil-kerjasama/details', 'PagesController@details');

//-----PRACTICE-----------------------------------------------------------------------------
Route::get('users', 'UserController@users');
Route::post('addUser', 'UserController@store');